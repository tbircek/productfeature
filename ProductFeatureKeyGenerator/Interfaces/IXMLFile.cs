﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ProductFeatureKeyGenerator.Interfaces
{
    /// <summary>
    /// Provides interface to xml file methods.
    /// </summary>
    public interface IXmlFile
    {
        /// <summary>
        /// Implements XML Create.
        /// </summary>
        string Create ( );

        /// <summary>
        /// Implements retrieval interface for available features. 
        /// </summary>
        /// <param name="fileName">What product to retrieve features for.</param>
        /// <returns>Returns features for <paramref name="fileName"/>.</returns>
        ObservableCollection<string> RetrieveFeaturesList ( string fileName );

        /// <summary>
        /// Implements retrieval interface for available product names.
        /// </summary>
        /// <param name="fileName">File location.</param>
        /// <returns>Returns all unique Product options from the <paramref name="fileName"/> possible.</returns>
        IList<string> RetrieveProductList ( string fileName );
    }
}
