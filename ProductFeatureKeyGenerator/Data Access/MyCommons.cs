﻿using System;
using System.IO;
using ProductFeatureKeyGenerator.Properties;

namespace ProductFeatureKeyGenerator.Data
{
    /// <summary>
    /// Holds project wide variables.
    /// </summary>
    internal static class MyCommons
    {
        /// <summary>
        /// The current view.
        /// </summary>
        internal static ViewModel MyViewModel { get; set; }

        /// <summary>
        /// Holds location of the local application data folder.
        /// </summary>
        internal static string FileOutputFolder
        {
            get
            {
                return Path.Combine ( Environment.GetFolderPath (
                                                Environment.SpecialFolder.LocalApplicationData ),
                                                Strings.MainWindowViewModel_ErrorFolder );
            }
        }
    }
}
