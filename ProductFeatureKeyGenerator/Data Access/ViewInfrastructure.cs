﻿using ProductFeatureKeyGenerator.MainModel;

namespace ProductFeatureKeyGenerator.Data
{
    /// <summary>
    /// Handles Infrastructure of MVVM model.
    /// Provides visibility across the project.
    /// </summary>
    public class ViewInfrastructure
    {
        /// <summary>
        /// MainWindow model.
        /// </summary>
        public MainModel.Model Model { get; private set; }

        /// <summary>
        /// MainWindow view.
        /// </summary>
        public ProductFeatureKeyGenerator.MainWindow View { get; private set; }

        /// <summary>
        /// MainWindow view model.
        /// </summary>
        public ViewModel ViewModel { get; private set; }

        /// <summary>
        /// Instantiate ViewInfrastructure.
        /// </summary>
        /// <param name="model">MainWindow model.</param>
        /// <param name="view">MainWindow view.</param>
        /// <param name="viewModel">MainWindow viewmodel.</param>
        public ViewInfrastructure (MainModel.Model model, ProductFeatureKeyGenerator.MainWindow view, ViewModel viewModel  )
        {
            this.Model = model;
            this.View = view;
            this.ViewModel = viewModel;
        }
    }
}
