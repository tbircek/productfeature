﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Resources;
using System.Xml;
using System.Xml.Linq;
using ProductFeatureKeyGenerator.Properties;

namespace ProductFeatureKeyGenerator.Operations
{

    /// <summary>
    /// Implements XML file to List methods.
    /// </summary>
    public class MyFeatureCollection : IDisposable
    {
        #region public properties

        /// <summary>
        /// Holds product types.
        /// </summary>
        public string Product { get; set; }

        /// <summary>
        /// Holds features names.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Holds register information of the feature.
        /// </summary>
        public int Register { get; private set; }

        /// <summary>
        /// Holds value to write to the register.
        /// </summary>
        public int Value { get; private set; }

        #endregion // public properties

        #region private properites

        /// <summary>
        /// Holds file location and name information.
        /// </summary>
        private string ResourceFile { get; set; }

        #endregion // private properites

        #region public methods

        /// <summary>
        /// Implements product list extraction method from the specified <paramref name="resourceFile"/>.
        /// </summary>
        /// <param name="resourceFile">Relative or absolute location and name of the resourceFile.</param>
        /// <returns>Returns a list that contains product available in the specified <paramref name="resourceFile"/>.</returns>
        public IList<MyFeatureCollection> BuildProductList ( string resourceFile )
        {
            this.ResourceFile = resourceFile;

            using ( Stream stream = GetResourceStream ( ) )
            {
                using ( XmlReader xmlrdr = new XmlTextReader ( stream ) )
                {
                    return
                        ( from featureLine in XDocument.Load ( xmlrdr ).Root.Elements ( )
                          select MyFeatureCollection.CreateNewProductList (
                          featureLine.Name.ToString ( )
                ) ).ToList ( );

                }
            }
        }

        /// <summary>
        /// Implements data extraction method from the specified <paramref name="resourceFile"/>.
        /// </summary>
        /// <param name="resourceFile">Product name to extract feature list.</param>
        /// <returns>Returns a list that contains specified product features.</returns>
        public IList<MyFeatureCollection> BuildFeatureList ( string resourceFile )
        {

            this.ResourceFile = resourceFile;

            using ( Stream stream = GetResourceStream ( ) )
            {
                using ( XmlReader xmlrdr = new XmlTextReader ( stream ) )
                {
                    return
                        ( from featureLine in XDocument.Load ( xmlrdr )
                                                        .Element ( Strings.Xml_Root )
                                                        .Element (ViewModel.MyViewModel.SelectedProduct)
                                                        .Elements ( Strings.Xml_ElementFeature )
                          select MyFeatureCollection.CreateNewFeatureList (
                          ( string ) featureLine.Attribute ( Strings.Xml_AttributeName ),
                          ( int ) featureLine.Attribute ( Strings.Xml_AttributeRegister ),
                          ( int ) featureLine.Attribute ( Strings.Xml_AttributeValue )
                     ) ).ToList ( );
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resourceFile"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        public IList<MyFeatureCollection> BuildFeatureList ( string resourceFile, string product )
        {

            this.ResourceFile = resourceFile;

            using ( Stream stream = GetResourceStream ( ) )
            {
                using ( XmlReader xmlrdr = new XmlTextReader ( stream ) )
                {
                    return
                        ( from featureLine in XDocument.Load ( xmlrdr )
                                                        .Element ( Strings.Xml_Root )
                                                        .Element ( product )
                                                        .Elements ( Strings.Xml_ElementFeature )
                          select MyFeatureCollection.CreateNewFeatureList (
                          ( string ) featureLine.Attribute ( Strings.Xml_AttributeName ),
                          ( int ) featureLine.Attribute ( Strings.Xml_AttributeRegister ),
                          ( int ) featureLine.Attribute ( Strings.Xml_AttributeValue )
                     ) ).ToList ( );
                }
            }
        }

        #endregion // public methods

        #region private methods

        /// <summary>
        /// Holds information about a feature.
        /// </summary>
        /// <param name="name">name of a feature.</param>
        /// <param name="register">register of a feature.</param>
        /// <param name="value">value of a feature's register value.</param>
        /// <returns></returns>
        private static MyFeatureCollection CreateNewFeatureList ( string name, int register, int value )
        {
            return new MyFeatureCollection
            {
                Name = name,
                Register = register,
                Value = value
            };
        }

        /// <summary>
        /// Holds a product name.
        /// </summary>
        /// <param name="product">name of the product.</param>
        /// <returns>Returns a collection of the product names.</returns>
        private static MyFeatureCollection CreateNewProductList ( string product )
        {
            return new MyFeatureCollection
            {
                Product = product
            };
        }

        /// <summary>
        /// Provides access to specified resourceFile.
        /// </summary>
        /// <returns>Returns a stream that contains specified resourceFile data.</returns>
        private Stream GetResourceStream ( )
        {
            Uri uri = new Uri ( this.ResourceFile, UriKind.RelativeOrAbsolute );

            StreamResourceInfo info = Application.GetResourceStream ( uri );
            if ( info == null || info.Stream == null )
            {
                throw new ArgumentException ( Strings.Validation_ResourceFileMissing + this.ResourceFile );
            }

            return info.Stream;
        }

        #endregion // private methods

        #region IDisposable Members

        /// <summary>
        /// Invoked when this object is being removed from the application
        /// and will be subject to garbage collection.
        /// </summary>
        public void Dispose ( )
        {
            this.OnDispose ( );
        }

        /// <summary>
        /// Child class can override this method to perform
        /// clean-up logic, such as removing event handlers.
        /// </summary>
        private void OnDispose ( ) { }

#if DEBUG

        /// <summary>
        /// Useful for ensuring that objects are properly garbage collected.
        /// </summary>
        ~MyFeatureCollection ( )
        {
            string msg = string.Format ( CultureInfo.InvariantCulture, "{0} Finalized.", this.GetType ( ).Name );
            System.Diagnostics.Debug.WriteLine ( msg );
        }
#endif
        #endregion
    }
}
