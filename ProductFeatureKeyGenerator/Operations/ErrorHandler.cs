﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Globalization;
using ProductFeatureKeyGenerator.Properties;

namespace ProductFeatureKeyGenerator.Operations
{
    /// <summary>
    /// Logs the errors can catch.
    /// </summary>
    internal static class ErrorHandler
    {
        #region Methods

        internal static Exception Log ( this Exception ex, string fileName = "Not specified" )
        {
            // Print file name to the debug window.
            Debug.WriteLineIf ( fileName != "Not specified", fileName );

            // Print to debug output
            Debug.WriteLine ( string.Format ( CultureInfo.InvariantCulture, "{0}", ex ), "Errors " );

            // Save to the file output folder
            //Trace.TraceError (string.Format ( CultureInfo.InvariantCulture, 
                                                            //MyResources.String

            return ex;
        }

        #endregion
    }
}
