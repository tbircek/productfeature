﻿using System;
using ProductFeatureKeyGenerator.Data;
using ProductFeatureKeyGenerator.MainModel;
using ProductFeatureKeyGenerator.Commands;

// FXCop warning CA1014.
[assembly: CLSCompliant ( true )]
namespace ProductFeatureKeyGenerator.Operations
{
    /// <summary>
    /// Handles views.
    /// Initializes commands.
    /// Initializes models.
    /// Initializes viewmodels.
    /// </summary>
    public class ViewFactory
    {
        #region Commands

        // Add the commands here.
        private EncryptDelegateCommand encryptDelegateCommand;
        // private FeaturesDelegateCommand featuresDelegateCommand;

        #endregion

        #region Infrastructure

        /// <summary>
        /// Creates new ViewInfrastructure.
        /// </summary>
        /// <returns></returns>
        public ViewInfrastructure Create ( )
        {
            // Initialize Commands here.
            this.encryptDelegateCommand = new EncryptDelegateCommand ( );
            // this.featuresDelegateCommand = new FeaturesDelegateCommand ( );

            // TODO: Initialize common properties here. Ex: Logging process.

            // Initialize new Model here.
            MainModel.Model model = new MainModel.Model ( );

            // Initialize mainview.
            ProductFeatureKeyGenerator.MainWindow view =
                            new ProductFeatureKeyGenerator.MainWindow ( );

            // Initialize new ViewModel here.
            ViewModel viewModel =
                new ViewModel ( model,
                    this.encryptDelegateCommand.Command ); //,
            // this.featuresDelegateCommand.Command);

            // Assign viewModel to MainViewModel.
            ViewModel.MyViewModel = viewModel;

            return new ViewInfrastructure ( model, view, viewModel );
        }

        #endregion
    }
}
