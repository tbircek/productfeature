﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Xml.Linq;
using ProductFeatureKeyGenerator.Interfaces;
using ProductFeatureKeyGenerator.Properties;

namespace ProductFeatureKeyGenerator.Operations
{
    /// <summary>
    /// Implements IXmlFile.
    /// </summary>
    public class XmlFile : IXmlFile, IDisposable
    {
        #region private properties

        private string serialNumber { get; set; }

        private string modelType { get; set; }

        private string feature { get; set; }

        #endregion

        #region private methods

        private string CreateXMLString ( )
        {

#if DEBUG
            new XDocument (
                new XElement ( Strings.Xml_Root,
                    new XElement ( Strings.Xml_ElementNumber, this.serialNumber ),
                    new XElement ( Strings.Xml_ElementModel, this.modelType ),
                    new XElement ( Strings.Xml_ElementFeature, this.feature )
                    )
                ).Save ( Strings.Xml_FileName );
#endif

            return new XDocument (
                 new XElement ( Strings.Xml_Root,
                   new XElement ( Strings.Xml_ElementNumber, this.serialNumber ),
                   new XElement ( Strings.Xml_ElementModel, this.modelType ),
                   new XElement ( Strings.Xml_ElementFeature, this.feature )
                   )
                ).ToString ( );
        }
        #endregion


        #region IXMLFile Members

        /// <summary>
        /// Create an xml string to to be encrypted.
        /// </summary>
        public string Create ( )
        {
            // retrieve the user input from the mainview.
            this.serialNumber = ViewModel.MyViewModel.SerialNumberTextBox;
            this.modelType = ViewModel.MyViewModel.SelectedProduct;
            this.feature = ViewModel.MyViewModel.SelectedRadioButton;

            return CreateXMLString ( );
        }

        /// <summary>
        /// Update the mainview with features that supported by the user selected product.
        /// </summary>
        /// <param name="FileName">Full name of the file with its location.</param>
        /// <returns>Returns a collection that contains supported features.</returns>
        public ObservableCollection<string> RetrieveFeaturesList ( string FileName )
        {
            MyFeatureCollection featureCollection = new MyFeatureCollection ( );
            ObservableCollection<string> newRetrieveFeatureNames = new ObservableCollection<string> ( );
            
            foreach ( var item in featureCollection.BuildFeatureList ( FileName ) )
            {
                newRetrieveFeatureNames.Add ( item.Name );
            }

            return newRetrieveFeatureNames;
        }

        /// <summary>
        /// Populate the mainview with supported products.
        /// </summary>
        /// <param name="FileName">Full name of the file with its location.</param>
        /// <returns>Returns a collection that contains supported products.</returns>
        public IList<string> RetrieveProductList ( string FileName )
        {

            MyFeatureCollection featureCollection = new MyFeatureCollection ( );
            List<string> newRetrieveProductNames = new List<string> ( );

            foreach ( var item in featureCollection.BuildProductList ( FileName ) )
            {
                newRetrieveProductNames.Add ( item.Product );
            }

            return newRetrieveProductNames;
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Invoked when this object is being removed from the application
        /// and will be subject to garbage collection.
        /// </summary>
        public void Dispose ( )
        {
            this.OnDispose ( );
        }

        /// <summary>
        /// Child class can override this method to perform
        /// clean-up logic, such as removing event handlers.
        /// </summary>
        protected virtual void OnDispose ( ) { }

#if DEBUG

        /// <summary>
        /// Useful for ensuring that objects are properly garbage collected.
        /// </summary>
        ~XmlFile ( )
        {
            string msg = string.Format (CultureInfo.InvariantCulture, "{0} Finalized.", this.GetType().Name);
            System.Diagnostics.Debug.WriteLine ( msg );
        }
#endif

        #endregion
    }
}
