﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ProductFeatureKeyGenerator.MainModel
{
    /// <summary>
    /// This class contains properties only.
    /// Handles the model only.
    /// </summary>
    public class Model
    {

        #region Public properties

        /// <summary>
        /// Program Title.
        /// </summary>
        public static string TitleLabel { get; set; }

        /// <summary>
        /// Serial number of the product.
        /// </summary>
        public string SerialNumberTextBox { get; set; }

        /// <summary>
        /// Serial number label.
        /// </summary>
        public static string SerialNumberLabel { get; set; }

        /// <summary>
        /// Model type label.
        /// </summary>
        public static string ProductLabel { get; set; }

        /// <summary>
        /// Features label.
        /// </summary>
        public static string FeaturesLabel { get; set; }

        /// <summary>
        /// Generate button label.
        /// </summary>
        public static string GenerateButtonLabel { get; set; }

        /// <summary>
        /// Holds the user selected combobox item which is a product.
        /// </summary>
        public string SelectedProduct { get; set; }

        /// <summary>
        /// Holds combobox items which contains the products.
        /// </summary>
        public static IList<string> CBoxItems { get; set; }

        /// <summary>
        /// Holds encrypted text.
        /// </summary>
        public string EncryptText { get; set; }

        /// <summary>
        /// Generate label for feature checkboxes.
        /// </summary>
        public ObservableCollection<string> FeatureLists { get; set; }

        /// <summary>
        /// Holds string value of the user selected feature value.
        /// </summary>
        public string SelectedRadioButton { get; set; }

        #endregion

    }
}
