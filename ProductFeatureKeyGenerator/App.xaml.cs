﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Threading;
using ProductFeatureKeyGenerator.Data;
using ProductFeatureKeyGenerator.Operations;
using ProductFeatureKeyGenerator.Properties;
using System.Security;
using System.Security.Permissions;


namespace ProductFeatureKeyGenerator
{

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        #region Startup

        /// <summary>
        /// Start up
        /// </summary>
        /// <param name="e">arguments</param>
        /// <remarks>FxCop warning CA2122: "FullTrust" will call the security check no matter what
        /// calls this method.</remarks>
        [PermissionSetAttribute ( SecurityAction.Demand, Name = "FullTrust" )]            
        protected override void OnStartup ( StartupEventArgs e )
        {
            #region Try to Catch All Unhandled Exceptions

            // Following code found on stackoverflow.com
            // http://stackoverflow.com/questions/10202987/in-c-sharp-how-to-collect-stack-trace-of-program-crash

            AppDomain currentDomain = default ( AppDomain );
            currentDomain = AppDomain.CurrentDomain;

            // Handler for unhandled exceptions.
            currentDomain.UnhandledException += GlobalUnhandledExceptionHandler;

            // Handler for exceptions in thread behind forms.
            Application.Current.DispatcherUnhandledException += GlobalThreadExceptionHandler;

            #endregion

            #region Verify LocalApplicationFolder does exists

            if ( !Directory.Exists ( ViewModel.FileOutputFolder ) )
            {
                Directory.CreateDirectory ( ViewModel.FileOutputFolder );
            }

            #endregion

            #region Create Logs

            //AddDebuggingTools ( );
#if DEBUG
            // new text streamer for console output.
            TextWriterTraceListener consoleOut = new TextWriterTraceListener ( System.Console.Out );
            Debug.Listeners.Add ( consoleOut );
#endif

            TextWriterTraceListener fileOut = new TextWriterTraceListener (
                                                    File.CreateText (
                                                         Path.Combine (
                                                                ViewModel.FileOutputFolder,
                                                                Strings.MainWindowViewModel_ErrorFileName ) ) );

            Debug.Listeners.Add ( fileOut );

            #endregion

            #region Application start

            base.OnStartup ( e );

            ViewFactory factory = new ViewFactory ( );
            ViewInfrastructure infrastructure = factory.Create ( );

            infrastructure.View.DataContext = infrastructure.ViewModel;
            infrastructure.View.Show ( );

            #endregion
        }

        #endregion

        #region Exit

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnExit ( ExitEventArgs e )
        {
            base.OnExit ( e );

            //TODO: add saving log files
            try
            {

            }
            catch ( NullReferenceException nre )
            {
                ErrorHandler.Log ( nre );
                Debug.Flush ( );
            }
            finally
            {
                Debug.Flush ( );
                Trace.Flush ( );
            }
        }

        #endregion

        #region Catch All Unhandled Exceptions

        // Following code found on stackoverflow.com
        // http://stackoverflow.com/questions/10202987/in-c-sharp-how-to-collect-stack-trace-of-program-crash
        private static void GlobalUnhandledExceptionHandler ( object sender, UnhandledExceptionEventArgs e )
        {
            Exception ex = default ( Exception );
            ex = ( Exception ) e.ExceptionObject;

            // Save to the fileOutputFolder and print to Debug window if the project build is in DEBUG.
            ErrorHandler.Log ( ex );
            Debug.Flush ( );
            Trace.Flush ( );
        }

        // Following code found on stackoverflow.com
        // http://stackoverflow.com/questions/10202987/in-c-sharp-how-to-collect-stack-trace-of-program-crash
        private static void GlobalThreadExceptionHandler ( object sender, DispatcherUnhandledExceptionEventArgs e )
        {
            Exception ex = default ( Exception );
            ex = e.Exception;

            // Save to the fileOutputFolder and print to Debug window if the project build is in DEBUG.
            ErrorHandler.Log ( ex );
            Debug.Flush ( );
            Trace.Flush ( );
        }

        #endregion

//        /// <summary>
//        /// Adds listeners to aid debugging.
//        /// </summary>
//        [SecurityCritical]
//        private static void AddDebuggingTools ( )
//        {

//            #region Create Logs

//#if DEBUG
//            // new text streamer for console output.
//            TextWriterTraceListener consoleOut = new TextWriterTraceListener ( System.Console.Out );
//            Debug.Listeners.Add ( consoleOut );
//#endif

//            TextWriterTraceListener fileOut = new TextWriterTraceListener (
//                                                    File.CreateText (
//                                                         Path.Combine (
//                                                                ViewModel.FileOutputFolder,
//                                                                Strings.MainWindowViewModel_ErrorFileName ) ) );

//            Debug.Listeners.Add ( fileOut );

//            #endregion

//        }
    }
}
