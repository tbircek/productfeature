﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using ProductFeatureKeyGenerator.Data;
using ProductFeatureKeyGenerator.Operations;
using ProductFeatureKeyGenerator.Properties;

namespace ProductFeatureKeyGenerator.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class EncryptDelegateCommand
    {

        /// <summary>
        /// default command.
        /// </summary>
        public DefaultCommand Command { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public EncryptDelegateCommand ( )
        {
            this.Command = new DefaultCommand ( this.ExecuteEncryption, this.CanExecute );

        }

        /// <summary>
        /// actual command encryption.
        /// </summary>
        /// <param name="unused"></param>
        public void ExecuteEncryption ( object unused )
        {
            try
            {

                XmlFile xml = new XmlFile ( );
                string stringToEncrpyt = xml.Create ( );

                if ( string.IsNullOrWhiteSpace ( stringToEncrpyt ) )
                {
                    throw new ArgumentException ( Strings.Validation_ArgumentExceptionMessage );
                }

                ViewModel.SavedSuccessfully = true;

                // update the user end of process.                
                ViewModel.MyViewModel.EncryptText = Strings.Validation_SaveSuccess;

            }
            catch ( ArgumentException nre )
            {
                // update the user failure.
                ViewModel.MyViewModel.EncryptText = Strings.Validation_SaveError;
                ViewModel.SavedSuccessfully = false;
                ErrorHandler.Log ( nre );
            }
        }

        /// <summary>
        /// Validates if the command can be executed.
        /// </summary>
        /// <param name="unused"></param>
        /// <returns>Returns true if every user inputs validates.</returns>
        public bool CanExecute ( object unused )
        {
            return
                ViewModel.MyViewModel.IsValid; // &&
                //ViewModel.MyViewModel.SelectedProduct != null &&
                //ViewModel.MyViewModel.SelectedRadioButton != null &&
               //  !( ViewModel.SavedSuccessfully );
        }
        
        ///// <summary>
        ///// http://stackoverflow.com/questions/321370/how-can-i-convert-a-hex-string-to-a-byte-array
        ///// </summary>
        ///// <param name="str">Hex string to convert to byte array.</param>
        ///// <returns>Byte array.</returns>
        //private static byte[] HexStringToByteArray ( string str )
        //{
        //    return Enumerable.Range ( 0, str.Length )
        //                     .Where ( x => x % 2 == 0 )
        //                     .Select ( x => Convert.ToByte ( str.Substring ( x, 2 ), 16 ) )
        //                     .ToArray ( );
        //}
    }
}