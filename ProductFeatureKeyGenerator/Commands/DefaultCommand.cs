﻿using System;
using System.Windows.Input;

namespace ProductFeatureKeyGenerator.Commands
{
    /// <summary>
    /// Default template for every other command created by this program.
    /// </summary>
    public class DefaultCommand : ICommand
    {

        #region Fields

        private readonly Action<object> execute;
        private readonly Predicate<object> canExecute;

        #endregion // Fields

        #region Public Methods

        /// <summary>
        /// Establishes Default structure for every command.
        /// </summary>
        /// <param name="execute">Action to execute.</param>
        /// <param name="canExecute">This is where blocking of the command occurs.</param>
        public DefaultCommand ( Action<object> execute, Predicate<object> canExecute )
        {

            if ( execute == null )
            {
                throw new ArgumentNullException ( "execute" );
            }

            if ( canExecute == null )
            {
                throw new ArgumentNullException ( "canExecute" );
            }

            this.execute = execute;
            this.canExecute = canExecute;
        }

        /// <summary>
        /// Establishes Default structure for every command.
        /// </summary>
        /// <param name="execute">Action to execute.</param>
        public DefaultCommand ( Action<object> execute )
        {
            if ( execute == null )
            {
                throw new ArgumentNullException ( "execute" );
            }

            this.execute = execute;
        }

        #endregion // Public Methods

        #region ICommand Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public bool CanExecute ( object parameter )
        {
            if ( this.canExecute == null )
            {
                return true;
            }
            else
            {
                return this.canExecute ( parameter );
            }
        }

        /// <summary>
        /// CanExecuteChanged eventhandler.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /// <summary>
        /// Establishes Default structure for every command.
        /// </summary>
        /// <param name="parameter">This is where blocking of the command occurs.</param>
        public void Execute ( object parameter )
        {
            this.execute ( parameter );
        }

        ///// <summary>
        ///// Calls OnCanExecuteChanged
        ///// </summary>
        //public void RaiseCanExecuteChanged ( )
        //{
        //    this.OnCanExecuteChanged ( );
        //}

        ///// <summary>
        ///// Handles if execute changes
        ///// </summary>
        //protected virtual void OnCanExecuteChanged ( )
        //{
        //    EventHandler handler = this.CanExecuteChanged;
        //    if ( handler != null )
        //    {
        //        handler.Invoke ( this, EventArgs.Empty );
        //    }
        //}

        #endregion
    }
}
