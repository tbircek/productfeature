﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Input;
using ProductFeatureKeyGenerator.Operations;
using ProductFeatureKeyGenerator.Properties;
using System.Xml;
using System.Windows.Data;
using System.IO;

namespace ProductFeatureKeyGenerator
{
    /// <summary>
    /// Hold main ViewModel.
    /// Handles all the commands.
    /// Handles all the validations.
    /// Handles its own disposal.
    /// </summary>
    public class ViewModel : INotifyPropertyChanged, IDisposable, IDataErrorInfo
    {

        #region Private variables

        /// <summary>
        /// Model variable.
        /// </summary>
        private readonly MainModel.Model model;

        #endregion

        #region Public properties

        /// <summary>
        /// Handles Title changes.
        /// </summary>
        public static string TitleLabel
        {
            get
            {
                return string.Format ( CultureInfo.InvariantCulture,
                                        Strings.MainWindowViewModel_MainTitle );
            }
        }

        /// <summary>
        /// The current view.
        /// </summary>
        internal static ViewModel MyViewModel { get; set; }

        /// <summary>
        /// Holds location of the local application data folder.
        /// </summary>
        internal static string FileOutputFolder
        {
            get
            {
                return Path.Combine ( Environment.GetFolderPath (
                                                Environment.SpecialFolder.LocalApplicationData ),
                                                Strings.MainWindowViewModel_ErrorFolder );
            }
        }

        /// <summary>
        /// Handles SerialNumberTextBox changes.
        /// </summary>
        public string SerialNumberTextBox
        {
            get
            {
                // user specified no information.
                return this.model.SerialNumberTextBox ?? string.Empty;
            }

            set
            {
                if ( this.model.SerialNumberTextBox != value )
                {
                    this.model.SerialNumberTextBox = value;

                    this.OnPropertyChanged ( "SerialNumberTextBox" );

                    // Clear EncryptText due to changes made by the user.
                    this.model.EncryptText = string.Empty;
                    this.OnPropertyChanged ( "EncryptText" );
                }
            }
        }

        /// <summary>
        /// Binds SerialNumberLabel to the resource file.
        /// </summary>
        public static string SerialNumberLabel
        {
            get
            {
                return Strings.MainWindowViewModel_SerialNumber;
            }
        }

        /// <summary>
        /// Binds ProductLabel to the resource file.
        /// </summary>
        public static string ProductLabel
        {
            get
            {
                return string.Format ( CultureInfo.InvariantCulture,
                                         Strings.MainWindowViewModel_ModelType );
            }
        }

        /// <summary>
        /// Binds FeaturesLabel to the resource file.
        /// </summary>
        public static string FeaturesLabel
        {
            get
            {
                return Strings.MainWindowViewModel_Feature;
            }
        }

        /// <summary>
        /// Retrieves Feature List from Features.xml file.
        /// </summary>
        public ObservableCollection<string> FeaturesList
        {
            get
            {
                if ( SelectedProduct != null )
                {
                    using ( Operations.XmlFile xmlFileAccess = new XmlFile ( ) )
                    {
                        return xmlFileAccess.RetrieveFeaturesList ( Strings.ViewModel_DataFileLocation );
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Returns combobox items.
        /// </summary>
        public static IList<string> CBoxItems
        {
            get
            {
                using ( Operations.XmlFile xmlFileAccess = new XmlFile ( ) )
                {
                    return xmlFileAccess.RetrieveProductList ( Strings.ViewModel_DataFileLocation );
                }
            }
        }

        /// <summary>
        /// Holds currently selected combobox item.
        /// </summary>
        private string currentlySelectedProduct;

        /// <summary>
        /// Returns and set currently selected combobox item.
        /// </summary>
        public string SelectedProduct
        {
            get
            {
                return currentlySelectedProduct;
            }
            set
            {
                if ( currentlySelectedProduct != value )
                {
                    currentlySelectedProduct = value;

                    // Do not need to populate CBox everytime currentlySelectedItem changes.
                    // Product changed. Reset the features.
                    this.model.SelectedRadioButton = null;
                    this.OnPropertyChanged ( "SelectedRadioButton" );

                    // Show appropriate featuresList per product line.
                    this.OnPropertyChanged ( "FeaturesList" );

                    // Clear EncryptText due to changes made by the user.
                    this.model.EncryptText = string.Empty;
                    this.OnPropertyChanged ( "EncryptText" );
                }
            }
        }

        /// <summary>
        /// holds encrpyted text.
        /// </summary>
        public string EncryptText
        {
            get
            {
                return this.model.EncryptText;
            }
            set
            {
                if ( this.model.EncryptText != value )
                {
                    // prevent unintentional multiple user inputs.
                    SavedSuccessfully = false;

                    this.model.EncryptText = value;
                    this.OnPropertyChanged ( "EncryptText" );

                    //EncryptCommand.CanExecute ( null );

                }
            }
        }

        /// <summary>
        /// Holds string value of the selected feature.
        /// </summary>
        public string SelectedRadioButton
        {
            get
            {
                return this.model.SelectedRadioButton;
            }
            set
            {
                if ( this.model.SelectedRadioButton != value )
                {
                    this.model.SelectedRadioButton = value;
                    this.OnPropertyChanged ( "SelectedRadioButton" );

                    // Clear EncryptText due to changes made by the user.
                    this.model.EncryptText = string.Empty;
                    this.OnPropertyChanged ( "EncryptText" );
                }
            }
        }
        
        /// <summary>
        /// holds whether command ran at least one time successfully.
        /// </summary>
        public static bool SavedSuccessfully { get; set; }

        #endregion

        #region Public commands

        // Add more commands here.

        /// <summary>
        /// Implements encryption command.
        /// </summary>
        public ICommand EncryptCommand { get; private set; }

        #endregion

        #region INotifyPropertyChanged Members

        /// <summary>
        /// This is where everything runs.
        /// Will need add every commands here.
        /// </summary>
        /// <param name="model">The model to operate.</param>
        /// <param name="encryptCommand">Creates encrypted string.</param>
        public ViewModel ( MainModel.Model model, ICommand encryptCommand )
        {

            this.model = model;
            this.EncryptCommand = encryptCommand;
        }

        /// <summary>
        /// Property changed event handler.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Handles Property changes.
        /// </summary>
        /// <param name="propertyName">The name of the property that changed.</param>
        protected virtual void OnPropertyChanged ( string propertyName )
        {

#if DEBUG
            Console.WriteLine ( "OnPropertyChanged ( propertyName: {0} ) processed.", propertyName );
#endif
            this.VerifyPropertyName ( propertyName );

            var handler = PropertyChanged;

            if ( handler != null )
            {
                handler ( this, new PropertyChangedEventArgs ( propertyName ) );
            }
        }

        #endregion

        #region Debugging Aide(s)

        /// <summary>
        /// Debugging aide only. 
        /// It will NOT run in Release version.
        /// Allows me to see if a property named incorrectly.
        /// </summary>
        /// <param name="propertyName"></param>
        [Conditional ( "DEBUG" )]
        [DebuggerStepThrough]
        public void VerifyPropertyName ( string propertyName )
        {
            if ( TypeDescriptor.GetProperties ( this )[ propertyName ] == null )
            {
                Debug.WriteLine ( string.Format ( CultureInfo.InvariantCulture,
                                    "Invalid property: {0}", propertyName ) );
            }
        }

        #endregion

        #region IDataErrorInfo Members

        /// <summary>
        /// holds the error strings.
        /// </summary>
        public string Error
        {
            get { return ( model as IDataErrorInfo ).Error; }
        }

        /// <summary>
        /// Provides an actual error messages for validations.
        /// Not sure if we need validations.
        /// </summary>
        /// <param name="columnName">FXCop mandated variable name holds actual propertyName.</param>
        /// <returns>Returns custom error messages for validations.</returns>
        public string this[ string columnName ]
        {
            get
            {
                return this.GetValidationError ( columnName );
            }
        }

        #endregion // IDataErrorInfo Members

        #region Validation

        /// <summary>
        /// Validated user inputs.
        /// </summary>
        static readonly string[] ValidatedProperties =
        {
            "SerialNumberTextBox",
            "SelectedProduct", 
            "SelectedRadioButton"
        };


        private string GetValidationError ( string columnName )
        {
            if ( Array.IndexOf ( ValidatedProperties, columnName ) < 0 )
            {
                return null;
            }

            string error = null;

            switch ( columnName )
            {
                case "SerialNumberTextBox":
                    error = this.ValidateSerialNumber ( );
                    break;

                case "SelectedProduct":
                    error = this.ValidateSelectedProduct ( );
                    break;

                case "SelectedRadioButton":
                    error = this.ValidateFeatureLists ( );
                    break;

                default:
                    error = ( model as IDataErrorInfo )[ columnName ];
                    Debug.Fail ( "Unexpected property is being validated." + columnName );
                    break;
            }

            CommandManager.InvalidateRequerySuggested ( );

            return error;
        }

        /// <summary>
        /// Warns the user to select a feature.
        /// </summary>
        /// <returns>Return null if the user selects a feature.</returns>
        private string ValidateFeatureLists ( )
        {
            if ( SelectedRadioButton == null )
            {
                return Strings.Validation_FeaturesMissing;
            }

            return null;
        }

        /// <summary>
        /// Returns true if every item validated with no errors.
        /// </summary>
        public bool IsValid
        {
            get
            {
                foreach ( string property in ValidatedProperties )
                {
                    if ( GetValidationError ( property ) != null )
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Validates the user entered Serial number.
        /// </summary>
        /// <returns>Returns an error string if validation fails.</returns>
        private string ValidateSerialNumber ( )
        {
            if ( IsStringEmpty ( this.SerialNumberTextBox ) )
            {
                return Strings.Validation_SerialNumberMissing;
            }
            else if ( !IsValidNumber ( this.SerialNumberTextBox ) )
            {
                return Strings.Validation_SerialNumberNaN;
            }
            else
            {
                if ( !( Convert.ToInt32 ( this.SerialNumberTextBox, CultureInfo.InvariantCulture ) > 0 ) )
                {
                    return Strings.Validation_SerialNumberInvalid;
                }
            }

            return null;
        }

        /// <summary>
        /// Checks if the value is an empty string.
        /// </summary>
        /// <param name="value">value to check.</param>
        /// <returns>Returns true if the string is an empty string.</returns>
        private static bool IsStringEmpty ( string value )
        {
            return string.IsNullOrWhiteSpace ( value );
        }

        /// <summary>
        /// Verifies value is a number.
        /// </summary>
        /// <param name="serialNumber">value to be validated.</param>
        /// <returns>Returns true if the value is a valid number, otherwise false.</returns>
        private static bool IsValidNumber ( string serialNumber )
        {
            string pattern = @"^[0-9]*$";

            return Regex.IsMatch ( serialNumber, //.Trim(), 
                                   pattern,
                                   RegexOptions.None |
                                   RegexOptions.IgnorePatternWhitespace );
        }

        /// <summary>
        /// Validates product combobox.
        /// </summary>
        /// <returns>Returns string if the validation failed, otherwise null.</returns>
        private string ValidateSelectedProduct ( )
        {
            if ( SelectedProduct == null )
            {
                SelectedRadioButton = null;
                return Strings.Validation_ProductMissing;
            }
            return null;
        }

        #endregion // Validation

        #region IDisposable Members

        /// <summary>
        /// Invoked when an object removed from the application.
        /// </summary>
        public void Dispose ( )
        {
            this.OnDispose ( );
        }

        /// <summary>
        /// Allow child classes to override this method.
        /// </summary>
        protected virtual void OnDispose ( ) { }

#if DEBUG
        /// <summary>
        /// verifies objects properly collected.
        /// </summary>
        ~ViewModel ( )
        {
            string msg = string.Format ( CultureInfo.InvariantCulture, "{0} Finalized", this.GetType ( ).Name );
            System.Diagnostics.Debug.WriteLine ( msg );
        }

#endif

        #endregion //IDisposable Members

    }
}