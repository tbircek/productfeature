﻿using System.Windows;
using System.Diagnostics;
using System.Windows.Controls;
using ProductFeatureKeyGenerator.Data;

namespace ProductFeatureKeyGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    sealed partial class MainWindow : Window
    {
        /// <summary>
        /// Main window
        /// </summary>
        public MainWindow ( )
        {
            InitializeComponent ( );
        }
    }
}
